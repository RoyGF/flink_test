package com.gemsdev.flinkapp.domain

import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

interface CharacterDataStore {
    fun getCharacterById(characterId: Int): Observable<Optional<CharacterEntity>>
    fun getCharacters(): Observable<List<CharacterEntity>>
}