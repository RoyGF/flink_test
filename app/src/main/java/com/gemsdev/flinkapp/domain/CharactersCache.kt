package com.gemsdev.flinkapp.domain

import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

interface CharactersCache {
    fun clear()
    fun saveCharacter(characterEntity: CharacterEntity)
    fun removeCharacter(characterEntity: CharacterEntity)
    fun saveAllCharacters(characterEntities: List<CharacterEntity>)
    fun getAllCharacters(): Observable<List<CharacterEntity>>
    fun getCharacter(characterId: Int): Observable<Optional<CharacterEntity>>
    fun isEmpty(): Boolean
}