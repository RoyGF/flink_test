package com.gemsdev.flinkapp.domain.use_cases

import com.gemsdev.flinkapp.domain.CharactersRepository
import com.gemsdev.flinkapp.domain.common.Transformer
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

class GetCharacterDetails(
    transformer: Transformer<Optional<CharacterEntity>>,
    private val charactersRepository: CharactersRepository
) : UseCase<Optional<CharacterEntity>>(transformer) {

    companion object {
        private const val PARAM_CHARACTER_ENTITY = "param:characterEntity"
    }

    fun getById(characterId: Int): Observable<Optional<CharacterEntity>> {
        val data = HashMap<String, Int>()
        data[PARAM_CHARACTER_ENTITY] = characterId
        return observable(data)
    }


    override fun createObservable(data: Map<String, Any>?): Observable<Optional<CharacterEntity>> {
        val movieId = data?.get(PARAM_CHARACTER_ENTITY)
        movieId?.let {
            return charactersRepository.getCharacter(it as Int)
        } ?: return Observable.error { IllegalArgumentException("CharacterId must be provided") }
    }

}


