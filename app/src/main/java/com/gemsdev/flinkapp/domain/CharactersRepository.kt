package com.gemsdev.flinkapp.domain

import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

interface CharactersRepository {
    fun getCharacters(): Observable<List<CharacterEntity>>
    fun getCharacter(characterId: Int): Observable<Optional<CharacterEntity>>

}