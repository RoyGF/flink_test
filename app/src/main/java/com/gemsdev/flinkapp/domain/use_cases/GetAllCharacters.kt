package com.gemsdev.flinkapp.domain.use_cases

import com.gemsdev.flinkapp.domain.CharactersRepository
import com.gemsdev.flinkapp.domain.common.Transformer
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import io.reactivex.Observable

open class GetAllCharacters constructor(
    transformer: Transformer<List<CharacterEntity>>,
    private val charactersRepository: CharactersRepository
) : UseCase<List<CharacterEntity>>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<List<CharacterEntity>> {
        return charactersRepository.getCharacters()
    }
}