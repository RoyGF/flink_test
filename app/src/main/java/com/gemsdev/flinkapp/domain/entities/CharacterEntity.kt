package com.gemsdev.flinkapp.domain.entities

data class CharacterEntity(
    val id: Int = -1,
    val name: String? = "",
    val status: String? = "",
    val species: String? = "",
    val type: String? = "",
    val gender: String? = "",
    val image: String? = "",
    val url: String? = "",
    val created: String? = ""
)