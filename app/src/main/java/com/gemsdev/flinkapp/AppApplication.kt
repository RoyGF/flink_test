package com.gemsdev.flinkapp

import android.app.Application
import com.gemsdev.flinkapp.dependency_injection.application.AppModule
import com.gemsdev.flinkapp.dependency_injection.application.DaggerMainComponent
import com.gemsdev.flinkapp.dependency_injection.application.MainComponent
import com.gemsdev.flinkapp.dependency_injection.character.CharacterListModule
import com.gemsdev.flinkapp.dependency_injection.character.CharacterListSubcomponent
import com.gemsdev.flinkapp.dependency_injection.character_detail.CharacterDetailSubComponent
import com.gemsdev.flinkapp.dependency_injection.character_detail.CharacterDetailsModule
import com.gemsdev.flinkapp.dependency_injection.data.DataModule
import com.gemsdev.flinkapp.dependency_injection.network.NetworkModule

class AppApplication : Application() {

    lateinit var mainComponent: MainComponent
    private var characterListComponent: CharacterListSubcomponent? = null
    private var characterDetailComponent: CharacterDetailSubComponent? = null

    override fun onCreate() {
        super.onCreate()
        initDependencies()
    }

    private fun initDependencies() {
        mainComponent = DaggerMainComponent.builder()
            .appModule(AppModule(applicationContext))
            .networkModule(NetworkModule("https://rickandmortyapi.com/", "1"))
            .dataModule(DataModule())
            .build()
    }

    fun createCharacterListComponent(): CharacterListSubcomponent {
        characterListComponent = mainComponent.plus(CharacterListModule())
        return characterListComponent!!
    }

    fun releaseCharacterListComponent() {
        characterListComponent = null
    }

    fun createCharacterDetailsComponent(): CharacterDetailSubComponent {
        characterDetailComponent = mainComponent.plus(CharacterDetailsModule())
        return characterDetailComponent!!
    }

    fun releaseCharacterDetailsComponent() {
        characterDetailComponent = null
    }
}