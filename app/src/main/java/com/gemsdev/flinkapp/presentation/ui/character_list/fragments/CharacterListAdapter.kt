package com.gemsdev.flinkapp.presentation.ui.character_list.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.gemsdev.flinkapp.R
import com.gemsdev.flinkapp.presentation.entities.Character
import kotlinx.android.synthetic.main.character_list_item.view.*

class CharacterListAdapter
constructor(
    private val onCharacterSelected:
        (Character, View) -> Unit
) : RecyclerView.Adapter<CharacterListAdapter.CharacterCellViewHolder>() {

    private val characters: MutableList<Character> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterCellViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.character_list_item, parent, false)
        return CharacterCellViewHolder(view)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun onBindViewHolder(holder: CharacterCellViewHolder, position: Int) {
        val character = characters[position]
        holder.bind(character, onCharacterSelected)
    }

    fun addCharacters(characters: List<Character>) {
        this.characters.addAll(characters)
        notifyDataSetChanged()
    }

    class CharacterCellViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(character: Character, listener: (Character, View) -> Unit) = with(itemView) {
            cell_name.text = character.name
            character.url?.let {
                cell_image.load(it)
            }
            setOnClickListener { listener(character, itemView) }
        }
    }
}