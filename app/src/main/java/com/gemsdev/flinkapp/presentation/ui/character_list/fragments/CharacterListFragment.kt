package com.gemsdev.flinkapp.presentation.ui.character_list.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gemsdev.flinkapp.AppApplication
import com.gemsdev.flinkapp.R
import com.gemsdev.flinkapp.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_character_list.*
import javax.inject.Inject

fun newCharacterListFragment() = CharacterListFragment()
val CHARACTER_LIST_FRAGMENT_TAG = CharacterListFragment::class.java.name

class CharacterListFragment : BaseFragment() {

    @Inject
    lateinit var factory: CharacterListVMFactory
    private lateinit var viewModel: CharacterListViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var characterListAdapter: CharacterListAdapter

    private var viewStateObserver = Observer<CharacterListViewState> {
        if (it != null) {
            handleViewState(it)
        }
    }

    private var errorStateObserver = Observer<Throwable?> { throwable ->
        throwable?.let {
            Toast.makeText(activity, throwable.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (this.activity?.application as AppApplication).createCharacterListComponent().inject(this)
        initViewModel()
        if (savedInstanceState == null) {
            viewModel.getCharacters()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_character_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.viewState.observe(viewLifecycleOwner, viewStateObserver)
        viewModel.errorState.observe(this, errorStateObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = character_progress
        characterListAdapter = CharacterListAdapter { character, view ->
            navigateToCharacterDetailScreen(character)
        }
        recyclerView = characters_recyclerview
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = characterListAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity?.application as AppApplication).releaseCharacterListComponent()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, factory).get(CharacterListViewModel::class.java)
    }

    private fun handleViewState(state: CharacterListViewState) {
        progressBar.visibility = if (state.showLoading) View.VISIBLE else View.GONE
        state.characters?.let { characterListAdapter.addCharacters(it) }
    }
}
