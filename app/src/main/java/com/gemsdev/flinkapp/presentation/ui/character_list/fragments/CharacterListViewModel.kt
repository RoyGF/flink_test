package com.gemsdev.flinkapp.presentation.ui.character_list.fragments

import androidx.lifecycle.MutableLiveData
import com.gemsdev.flinkapp.base.BaseViewModel
import com.gemsdev.flinkapp.common.SingleLiveEvent
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.use_cases.GetAllCharacters
import com.gemsdev.flinkapp.presentation.entities.Character
import javax.inject.Inject

class CharacterListViewModel
@Inject constructor(
    private val getAllCharacters: GetAllCharacters,
    private val characterEntityCharacterMapper: Mapper<CharacterEntity, Character>
) : BaseViewModel() {
    var viewState: MutableLiveData<CharacterListViewState> = MutableLiveData()
    var errorState: SingleLiveEvent<Throwable?> = SingleLiveEvent()

    init {
        viewState.value = CharacterListViewState()
    }

    fun getCharacters() {
        addDisposable(getAllCharacters.observable()
            .flatMap { characterEntityCharacterMapper.observable(it) }
            .subscribe({ characters ->
                viewState.value?.let {
                    val newState =
                        this.viewState.value?.copy(showLoading = false, characters = characters)
                    this.viewState.value = newState
                    this.errorState.value = null
                }
            }, {
                viewState.value = viewState.value?.copy(showLoading = false)
                errorState.value = it
            }))
    }
}