package com.gemsdev.flinkapp.presentation.ui.character_list.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.use_cases.GetAllCharacters
import com.gemsdev.flinkapp.presentation.entities.Character

class CharacterListVMFactory(
    private val useCase: GetAllCharacters,
    private val mapper: Mapper<CharacterEntity, Character>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CharacterListViewModel(useCase, mapper) as T
    }

}