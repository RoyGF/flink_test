package com.gemsdev.flinkapp.presentation.ui.character_list.fragments

import com.gemsdev.flinkapp.presentation.entities.Character

data class CharacterListViewState(
    var showLoading: Boolean = true,
    var characters: List<Character>? = null
)