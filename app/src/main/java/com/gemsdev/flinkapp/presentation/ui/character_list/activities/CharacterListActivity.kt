package com.gemsdev.flinkapp.presentation.ui.character_list.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gemsdev.flinkapp.R
import com.gemsdev.flinkapp.presentation.ui.character_list.fragments.CHARACTER_LIST_FRAGMENT_TAG
import com.gemsdev.flinkapp.presentation.ui.character_list.fragments.newCharacterListFragment

class CharacterListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_list)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.fragment_container,
                    newCharacterListFragment(),
                    CHARACTER_LIST_FRAGMENT_TAG
                )
                .commitNow()
            title = getString(R.string.title)
        }
    }
}
