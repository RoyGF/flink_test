package com.gemsdev.flinkapp.presentation.ui.character_detail.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import coil.api.load
import com.gemsdev.flinkapp.AppApplication
import com.gemsdev.flinkapp.R
import com.gemsdev.flinkapp.databinding.ActivityCharacterDetailBinding
import com.gemsdev.flinkapp.presentation.ui.character_detail.factory.CharacterDetailVMFactory
import com.gemsdev.flinkapp.presentation.ui.character_detail.state.CharacterDetailsViewState
import com.gemsdev.flinkapp.presentation.ui.character_detail.view_model.CharacterDetailsViewModel
import javax.inject.Inject

class CharacterDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: CharacterDetailVMFactory

    private lateinit var characterDetailsViewModel: CharacterDetailsViewModel
    private lateinit var binding: ActivityCharacterDetailBinding

    companion object {
        const val CHARACTER_ID: String = "extra_character_id"

        fun newIntent(context: Context, characterId: Int): Intent {
            val intent = Intent(context, CharacterDetailActivity::class.java)
            intent.putExtra(CHARACTER_ID, characterId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_character_detail)
        (application as AppApplication).createCharacterDetailsComponent().inject(this)
        val characterId = intent.extras!!.getInt(CHARACTER_ID, 0)
        factory.characterId = characterId

        characterDetailsViewModel =
            ViewModelProviders.of(this, factory).get(CharacterDetailsViewModel::class.java)

        observeViewState()
        characterDetailsViewModel.getCharacterDetails()
    }

    private fun observeViewState() {
        characterDetailsViewModel.viewState.observe(this, Observer { viewState ->
            handleViewState(viewState)
        })
        characterDetailsViewModel.errorState.observe(this, Observer { throwable ->
            Toast.makeText(this, throwable.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun handleViewState(state: CharacterDetailsViewState?) {
        if (state == null)
            return
        state.imageURL?.let {
            binding.imageCharacter.load(it)
        }
        binding.textNameCharacter.text = state.name
        binding.textGenderCharacter.text = state.gender
        binding.textTypeCharacter.text = state.type
        binding.characterDetailProgressBar.visibility =
            if (state.isLoading) View.VISIBLE else View.GONE
    }
}
