package com.gemsdev.flinkapp.presentation.ui.character_detail.state

data class CharacterDetailsViewState(
    var isLoading: Boolean = true,
    var name: String? = "",
    var type: String? = "",
    var gender: String? = "",
    var imageURL: String? = ""
)