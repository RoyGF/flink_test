package com.gemsdev.flinkapp.presentation.ui.character_detail.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.use_cases.GetCharacterDetails
import com.gemsdev.flinkapp.presentation.entities.Character
import com.gemsdev.flinkapp.presentation.ui.character_detail.view_model.CharacterDetailsViewModel

class CharacterDetailVMFactory(
    private val getCharacterDetails: GetCharacterDetails,
    private val mapper: Mapper<CharacterEntity, Character>
) : ViewModelProvider.Factory {

    var characterId: Int = -1

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CharacterDetailsViewModel(getCharacterDetails, mapper, characterId) as T
    }
}
