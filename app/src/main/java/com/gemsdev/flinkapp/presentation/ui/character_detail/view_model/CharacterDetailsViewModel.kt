package com.gemsdev.flinkapp.presentation.ui.character_detail.view_model

import androidx.lifecycle.MutableLiveData
import com.gemsdev.flinkapp.base.BaseViewModel
import com.gemsdev.flinkapp.common.SingleLiveEvent
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.use_cases.GetCharacterDetails
import com.gemsdev.flinkapp.presentation.entities.Character
import com.gemsdev.flinkapp.presentation.ui.character_detail.state.CharacterDetailsViewState

class CharacterDetailsViewModel(
    private val getCharacterDetails: GetCharacterDetails,
    private val mapper: Mapper<CharacterEntity, Character>,
    private val characterId: Int
) : BaseViewModel() {

    lateinit var characterEntity: CharacterEntity
    var viewState: MutableLiveData<CharacterDetailsViewState> = MutableLiveData()
    var errorState: SingleLiveEvent<Throwable> = SingleLiveEvent()

    init {
        viewState.value = CharacterDetailsViewState(isLoading = true)
    }

    fun getCharacterDetails() {
        addDisposable(
            getCharacterDetails.getById(characterId).map {
                it.value?.let {
                    characterEntity = it
                    mapper.mapFrom(characterEntity)
                } ?: run {
                    throw Throwable("Could not retch Character :(")
                }
            }.subscribe(
                {
                    onCharacterDetailReceived(it)
                }, {
                    errorState.value = it
                }
            )
        )
    }

    private fun onCharacterDetailReceived(character: Character) {
        val newViewState = viewState.value?.copy(
            isLoading = false,
            name = character.name,
            type = character.type,
            gender = character.gender,
            imageURL = character.image
        )
        viewState.value = newViewState
    }


}