package com.gemsdev.flinkapp.presentation.entities

data class Character(
    val id: Int = -1,
    val name: String? = "",
    val status: String? = "",
    val species: String? = "",
    val type: String? = "",
    val gender: String? = "",
    val image: String? = "",
    val url: String? = "",
    val created: String? = ""
)