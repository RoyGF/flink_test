package com.gemsdev.flinkapp.base

import android.annotation.SuppressLint
import android.app.ActivityOptions
import androidx.fragment.app.Fragment
import com.gemsdev.flinkapp.presentation.entities.Character
import com.gemsdev.flinkapp.presentation.ui.character_detail.activities.CharacterDetailActivity

open class BaseFragment : Fragment() {

    @SuppressLint("NewApi")
    protected fun navigateToCharacterDetailScreen(character: Character) {
        var activityOptions: ActivityOptions? = null

        startActivity(
            CharacterDetailActivity.newIntent(
                requireContext(),
                character.id
            ), activityOptions?.toBundle()
        )
        activity?.overridePendingTransition(0, 0)
    }

}