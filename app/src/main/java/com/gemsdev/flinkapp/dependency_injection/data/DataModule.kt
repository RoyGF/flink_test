package com.gemsdev.flinkapp.dependency_injection.data

import android.content.Context
import androidx.room.Room
import com.gemsdev.flinkapp.data.api.Api
import com.gemsdev.flinkapp.data.db.AppDatabase
import com.gemsdev.flinkapp.data.db.RoomAppCache
import com.gemsdev.flinkapp.data.mappers.CharacterDataEntityMapper
import com.gemsdev.flinkapp.data.mappers.CharacterEntityDataMapper
import com.gemsdev.flinkapp.data.repository.CachedCharactersDataStore
import com.gemsdev.flinkapp.data.repository.CharactersRepositoryImpl
import com.gemsdev.flinkapp.data.repository.RemoteCharactersDataStore
import com.gemsdev.flinkapp.dependency_injection.DependencyInjection
import com.gemsdev.flinkapp.domain.CharacterDataStore
import com.gemsdev.flinkapp.domain.CharactersCache
import com.gemsdev.flinkapp.domain.CharactersRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "app_database")
            .allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun provideCharacterRepository(
        api: Api,
        @Named(DependencyInjection.inMemoryCache) cache: CharactersCache,
        characterDataMapper: CharacterDataEntityMapper
    ): CharactersRepository {
        return CharactersRepositoryImpl(api, cache, characterDataMapper)
    }

    @Singleton
    @Provides
    @Named(DependencyInjection.inMemoryCache)
    fun provideInMemoryCharactersCache(
        database: AppDatabase, entityToDataMapper: CharacterEntityDataMapper,
        dataToEntityMapper: CharacterDataEntityMapper
    ): CharactersCache {
        return RoomAppCache(database, entityToDataMapper, dataToEntityMapper)
    }

    @Singleton
    @Provides
    @Named(DependencyInjection.cachedDataStore)
    fun provideCachedCharactersDataStore(charactersCache: CharactersCache): CharacterDataStore {
        return CachedCharactersDataStore(charactersCache)
    }

    @Singleton
    @Provides
    @Named(DependencyInjection.remoteDataStore)
    fun provideRemoteCharacterDataStore(
        api: Api,
        characterDataMapper: CharacterDataEntityMapper
    ): CharacterDataStore {
        return RemoteCharactersDataStore(api, characterDataMapper)
    }


}