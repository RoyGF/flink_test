package com.gemsdev.flinkapp.dependency_injection.character

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class CharacterListScope