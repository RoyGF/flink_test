package com.gemsdev.flinkapp.dependency_injection.character

import com.gemsdev.flinkapp.data.mappers.CharacterEntitiyCharacterMapper
import com.gemsdev.flinkapp.domain.CharactersRepository
import com.gemsdev.flinkapp.domain.use_cases.GetAllCharacters
import com.gemsdev.flinkapp.presentation.common.AsyncTransformer
import com.gemsdev.flinkapp.presentation.ui.character_list.fragments.CharacterListVMFactory
import dagger.Module
import dagger.Provides

@Module
class CharacterListModule {
    @CharacterListScope
    @Provides
    fun provideGetCharactersUseCase(charactersRepository: CharactersRepository): GetAllCharacters {
        return GetAllCharacters(AsyncTransformer(), charactersRepository)
    }

    @CharacterListScope
    @Provides
    fun provideCharacterListVMFactory(
        useCase: GetAllCharacters,
        mapper: CharacterEntitiyCharacterMapper
    ): CharacterListVMFactory {
        return CharacterListVMFactory(useCase, mapper)
    }
}