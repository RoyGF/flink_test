package com.gemsdev.flinkapp.dependency_injection.character

import com.gemsdev.flinkapp.presentation.ui.character_list.fragments.CharacterListFragment
import dagger.Subcomponent

@CharacterListScope
@Subcomponent(modules = [CharacterListModule::class])
interface CharacterListSubcomponent {
    fun inject(characterListFragment: CharacterListFragment)
}