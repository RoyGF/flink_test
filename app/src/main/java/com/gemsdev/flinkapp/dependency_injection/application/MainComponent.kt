package com.gemsdev.flinkapp.dependency_injection.application

import com.gemsdev.flinkapp.dependency_injection.character.CharacterListModule
import com.gemsdev.flinkapp.dependency_injection.character.CharacterListSubcomponent
import com.gemsdev.flinkapp.dependency_injection.character_detail.CharacterDetailSubComponent
import com.gemsdev.flinkapp.dependency_injection.character_detail.CharacterDetailsModule
import com.gemsdev.flinkapp.dependency_injection.data.DataModule
import com.gemsdev.flinkapp.dependency_injection.network.NetworkModule
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


const val SCHEDULER_MAIN_THREAD = "scheduler_main_thread"
const val SCHEDULER_IO = "scheduler_io"

@Singleton
@Component(
    modules = [(AppModule::class), (NetworkModule::class), (DataModule::class), (AndroidSupportInjectionModule::class)]
)
interface MainComponent {
    fun plus(characterListModule: CharacterListModule): CharacterListSubcomponent
    fun plus(CharacterDetailModule: CharacterDetailsModule) : CharacterDetailSubComponent
}
