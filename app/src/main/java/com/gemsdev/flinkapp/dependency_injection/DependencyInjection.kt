package com.gemsdev.flinkapp.dependency_injection

class DependencyInjection {
    companion object {
        const val inMemoryCache = "inMemoryCache"
        const val charactersCache = "charactersCache"
        const val remoteDataStore = "remoteDataStore"
        const val cachedDataStore = "cachedDataStore"
    }
}