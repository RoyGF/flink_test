package com.gemsdev.flinkapp.dependency_injection.character_detail

import com.gemsdev.flinkapp.presentation.ui.character_detail.activities.CharacterDetailActivity
import dagger.Subcomponent

@CharacterDetailsScope
@Subcomponent(modules = [CharacterDetailsModule::class])
interface CharacterDetailSubComponent {
    fun inject(characterDetailActivity: CharacterDetailActivity)
}