package com.gemsdev.flinkapp.dependency_injection.fragments

import com.gemsdev.flinkapp.presentation.ui.character_list.fragments.CharacterListFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface CharacterListFragmentSubcomponent : AndroidInjector<CharacterListFragment> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CharacterListFragment>()
}

