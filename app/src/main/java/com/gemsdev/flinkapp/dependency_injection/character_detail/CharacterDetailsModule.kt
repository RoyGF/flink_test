package com.gemsdev.flinkapp.dependency_injection.character_detail

import com.gemsdev.flinkapp.data.mappers.CharacterEntitiyCharacterMapper
import com.gemsdev.flinkapp.domain.CharactersRepository
import com.gemsdev.flinkapp.domain.use_cases.GetCharacterDetails
import com.gemsdev.flinkapp.presentation.common.AsyncTransformer
import com.gemsdev.flinkapp.presentation.ui.character_detail.factory.CharacterDetailVMFactory
import dagger.Module
import dagger.Provides

@Module
class CharacterDetailsModule {

    @Provides
    fun getGetCharacterDetailUseCase(charactersRepository: CharactersRepository): GetCharacterDetails {
        return GetCharacterDetails(AsyncTransformer(), charactersRepository)
    }

    @Provides
    fun provideCharacterDetailsVMFactory(
        getCharacterDetails: GetCharacterDetails,
        mapper: CharacterEntitiyCharacterMapper
    ): CharacterDetailVMFactory {
        return CharacterDetailVMFactory(getCharacterDetails, mapper)
    }


}