package com.gemsdev.flinkapp.dependency_injection.character_detail

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class CharacterDetailsScope