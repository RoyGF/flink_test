package com.gemsdev.flinkapp.data.repository

import com.gemsdev.flinkapp.domain.CharacterDataStore
import com.gemsdev.flinkapp.domain.CharactersCache
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable


class CachedCharactersDataStore(private val charactersCache: CharactersCache) : CharacterDataStore {

    override fun getCharacterById(characterId: Int): Observable<Optional<CharacterEntity>> {
        return charactersCache.getCharacter(characterId)
    }

    override fun getCharacters(): Observable<List<CharacterEntity>> {
        return charactersCache.getAllCharacters()
    }

}