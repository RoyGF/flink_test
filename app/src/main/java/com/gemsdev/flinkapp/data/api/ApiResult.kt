package com.gemsdev.flinkapp.data.api

import com.gemsdev.flinkapp.data.entities.CharacterData
import com.google.gson.annotations.SerializedName

class ApiResult {
    lateinit var info: Info
    @SerializedName("results")
    lateinit var characters: List<CharacterData>
}