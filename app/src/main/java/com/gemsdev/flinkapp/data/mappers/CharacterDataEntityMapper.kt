package com.gemsdev.flinkapp.data.mappers

import com.gemsdev.flinkapp.data.entities.CharacterData
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import javax.inject.Inject

class CharacterDataEntityMapper @Inject
constructor() : Mapper<CharacterData, CharacterEntity>() {

    override fun mapFrom(from: CharacterData): CharacterEntity {
        return CharacterEntity(
            id = from.id,
            name = from.name,
            status = from.status,
            species = from.species,
            type = from.type,
            gender = from.gender,
            image = from.image,
            url = from.image,
            created = from.created
        )
    }
}