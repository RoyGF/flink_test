package com.gemsdev.flinkapp.data.repository

import com.gemsdev.flinkapp.data.api.Api
import com.gemsdev.flinkapp.data.entities.CharacterData
import com.gemsdev.flinkapp.domain.CharacterDataStore
import com.gemsdev.flinkapp.domain.CharactersCache
import com.gemsdev.flinkapp.domain.CharactersRepository
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

class CharactersRepositoryImpl(
    api: Api,
    private val cache: CharactersCache,
    characterDataMapper: Mapper<CharacterData, CharacterEntity>

) : CharactersRepository {

    private val memoryDataStore: CharacterDataStore
    private val remoteDataStore: CharacterDataStore

    init {
        memoryDataStore = CachedCharactersDataStore(cache)
        remoteDataStore = RemoteCharactersDataStore(api, characterDataMapper)
    }

    override fun getCharacters(): Observable<List<CharacterEntity>> {
        return if (!cache.isEmpty()) {
            return memoryDataStore.getCharacters()
        } else {
            remoteDataStore.getCharacters().doOnNext {
                cache.saveAllCharacters(it)
            }
        }
    }

    override fun getCharacter(characterId: Int): Observable<Optional<CharacterEntity>> {
        return remoteDataStore.getCharacterById(characterId)
    }

}