package com.gemsdev.flinkapp.data.db

import androidx.room.*
import com.gemsdev.flinkapp.data.entities.CharacterData

@Dao
interface AppDatabaseDao {
    @Query("SELECT * FROM characters")
    fun getCharacters(): List<CharacterData>

    @Query("SELECT * FROM characters WHERE id=:characterId")
    fun get(characterId: Int): CharacterData?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCharacter(character: CharacterData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllCharacters(characters: List<CharacterData>)

    @Delete
    fun removeCharacter(character: CharacterData)

    @Query("DELETE FROM characters")
    fun clearDatabase()
}