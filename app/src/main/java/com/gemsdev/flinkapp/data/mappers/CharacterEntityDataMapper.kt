package com.gemsdev.flinkapp.data.mappers

import com.gemsdev.flinkapp.data.entities.CharacterData
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import javax.inject.Inject

class CharacterEntityDataMapper @Inject
constructor() : Mapper<CharacterEntity, CharacterData>() {

    override fun mapFrom(from: CharacterEntity): CharacterData {
        return CharacterData(
            id = from.id,
            name = from.name,
            status = from.status,
            species = from.species,
            type = from.type,
            gender = from.gender,
            image = from.image,
            url = from.image,
            created = from.created
        )
    }

}