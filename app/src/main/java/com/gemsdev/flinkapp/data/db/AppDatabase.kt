package com.gemsdev.flinkapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gemsdev.flinkapp.data.entities.CharacterData

@Database(entities = arrayOf(CharacterData::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCharactersDao(): AppDatabaseDao
}