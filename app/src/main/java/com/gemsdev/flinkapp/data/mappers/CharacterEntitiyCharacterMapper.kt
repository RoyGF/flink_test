package com.gemsdev.flinkapp.data.mappers

import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.presentation.entities.Character
import javax.inject.Inject

class CharacterEntitiyCharacterMapper
@Inject constructor() : Mapper<CharacterEntity, Character>() {

    override fun mapFrom(from: CharacterEntity): Character {
        return Character(
            id = from.id,
            name = from.name,
            status = from.status,
            species = from.species,
            type = from.type,
            gender = from.gender,
            image = from.image,
            url = from.url,
            created = from.created
        )
    }
}