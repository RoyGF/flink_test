package com.gemsdev.flinkapp.data.api

data class Info (
    val count: Long,
    val pages: Long,
    val next: String,
    val prev: String? = null
){}