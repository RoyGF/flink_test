package com.gemsdev.flinkapp.data.db

import androidx.lifecycle.LiveData
import com.gemsdev.flinkapp.data.entities.CharacterData
import com.gemsdev.flinkapp.domain.CharactersCache
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable
import java.util.concurrent.Future

class RoomAppCache(
    database: AppDatabase,
    private val entityToDataMapper: Mapper<CharacterEntity, CharacterData>,
    private val dataToEntityMapper: Mapper<CharacterData, CharacterEntity>
) : CharactersCache {

    private val dao: AppDatabaseDao = database.getCharactersDao()

    override fun clear() {
        dao.clearDatabase()
    }

    override fun saveCharacter(characterEntity: CharacterEntity) {
        dao.saveCharacter(entityToDataMapper.mapFrom(characterEntity))
    }

    override fun removeCharacter(characterEntity: CharacterEntity) {
        dao.removeCharacter(entityToDataMapper.mapFrom(characterEntity))
    }

    override fun saveAllCharacters(characterEntities: List<CharacterEntity>) {
        dao.saveAllCharacters(characterEntities.map { entityToDataMapper.mapFrom(it) })
    }

    override fun getAllCharacters(): Observable<List<CharacterEntity>> {
        return Observable.fromCallable {
            dao.getCharacters().map { dataToEntityMapper.mapFrom(it) }
        }
    }

    override fun getCharacter(characterId: Int): Observable<Optional<CharacterEntity>> {
        return Observable.fromCallable {
            val characterData = dao.get(characterId)
            characterData?.let {
                Optional.of(dataToEntityMapper.mapFrom(it))
            } ?: Optional.empty()
        }
    }

    override fun isEmpty(): Boolean {
        return dao.getCharacters().isEmpty()
    }
}