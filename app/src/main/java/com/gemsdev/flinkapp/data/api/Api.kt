package com.gemsdev.flinkapp.data.api

import com.gemsdev.flinkapp.data.entities.CharacterData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    @GET("/api/character/")
    fun getCharacters(): Observable<ApiResult>

    @GET("/api/character/{id}")
    fun getCharacterById(@Path("id") characterId: Int): Observable<CharacterData>
}