package com.gemsdev.flinkapp.data.repository

import com.gemsdev.flinkapp.domain.CharactersCache
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

class MemoryCharactersCache : CharactersCache {

    private val characters: LinkedHashMap<Int, CharacterEntity> = LinkedHashMap()

    override fun clear() {
        characters.clear()
    }

    override fun saveCharacter(characterEntity: CharacterEntity) {
        characters[characterEntity.id] = characterEntity
    }

    override fun removeCharacter(characterEntity: CharacterEntity) {
        characters.remove(characterEntity.id)
    }

    override fun saveAllCharacters(characterEntities: List<CharacterEntity>) {
        characterEntities.forEach { characterEntity ->
            this.characters[characterEntity.id] = characterEntity
        }
    }

    override fun getAllCharacters(): Observable<List<CharacterEntity>> {
        return Observable.just(characters.values.toList())
    }

    override fun getCharacter(characterId: Int): Observable<Optional<CharacterEntity>> {
        return Observable.just(Optional.of(characters[characterId]))
    }

    override fun isEmpty(): Boolean {
        return characters.isEmpty()
    }

}