package com.gemsdev.flinkapp.data.repository

import com.gemsdev.flinkapp.data.api.Api
import com.gemsdev.flinkapp.data.entities.CharacterData
import com.gemsdev.flinkapp.domain.CharacterDataStore
import com.gemsdev.flinkapp.domain.Mapper
import com.gemsdev.flinkapp.domain.entities.CharacterEntity
import com.gemsdev.flinkapp.domain.entities.Optional
import io.reactivex.Observable

class RemoteCharactersDataStore(
    private val api: Api,
    private val characterDataMapper: Mapper<CharacterData, CharacterEntity>
) : CharacterDataStore {

    override fun getCharacterById(characterId: Int): Observable<Optional<CharacterEntity>> {
        return api.getCharacterById(characterId).flatMap { detailedData ->
            Observable.just(Optional.of(characterDataMapper.mapFrom(detailedData)))
        }
    }

    override fun getCharacters(): Observable<List<CharacterEntity>> {
        return api.getCharacters().map { results ->
            results.characters.map { characterDataMapper.mapFrom(it) }
        }
    }
}