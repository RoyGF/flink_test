# Description:
- First Screen Shows a List of Characters from Rick and Morty
- Second Screen opens Character's Detail

## Architecture:
- MVVM
- S.O.L.I.D. Principles
- Clean Architecture

## Libraries Used:
- Dagger 2 for dependency injection
- Data Binding
- Room for Data Persistence
- Retrofit for API Endpoint Fetching

## Developing Order:
1) Libraries Implementations on gradle settings
2) Retrofit Implementation
3) Room Database Implementation
4) Finish Data Layer
5) Use Cases Implementation
6) Finish Domain Layer
7) Dependency Injection Implementation for Data and Domain layers
8) Mappers for Entities between Data and Domain Layers
9) Character List Screen Activity
10) Character List Fragment 
11) Character List View Model / states and Factory
12) Character Detail Activity
13) Character Detail ViewModel / States and Factory
14) Finish Presentation Layer
15) Add Readme

## Notes:
- Implementing Clean Architecture Layers were super time consuming. With more time I would improve some implementations. 

## ScreenShots:
![screenshot01](/uploads/fc90d66ffd607086f7c061e9f9584e61/screenshot01.png)

![screenshot02](/uploads/3df5d6e0b02728df375e08dcce2a79f4/screenshot02.png)
